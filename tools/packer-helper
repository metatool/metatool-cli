#!/bin/bash

PATH=$PATH:/usr/lib/vmware-viperl/apps/vm
PATH=$PATH:$HOME/vghetto-scripts/perl

# this quick helper is intended for running on an OS X installation

prereq_errors=0

initializeANSI() {
  # function sourced from 
  # http://intuitive.com/wicked/scripts/011-colors.txt

  esc="\033"

  blackf="${esc}[30m";   redf="${esc}[31m";    greenf="${esc}[32m"
  yellowf="${esc}[33m"   bluef="${esc}[34m";   purplef="${esc}[35m"
  cyanf="${esc}[36m";    whitef="${esc}[37m"
  
  blackb="${esc}[40m";   redb="${esc}[41m";    greenb="${esc}[42m"
  yellowb="${esc}[43m"   blueb="${esc}[44m";   purpleb="${esc}[45m"
  cyanb="${esc}[46m";    whiteb="${esc}[47m"

  boldon="${esc}[1m";    boldoff="${esc}[22m"
  italicson="${esc}[3m"; italicsoff="${esc}[23m"
  ulon="${esc}[4m";      uloff="${esc}[24m"
  invon="${esc}[7m";     invoff="${esc}[27m"

  reset="${esc}[0m"
}

# init the color sequences
initializeANSI

# thank you: http://stackoverflow.com/questions/5014632/how-can-i-parse-a-yaml-file-from-a-linux-shell-script
parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

check_tool_installed() {
  tool_name=$1
  tool_cmd=$2
  tool_website=$3
  # check if tools is installed and/or in path
  debug_output "${yellowf}Checking for ${tool_name}${reset}"
  if [[ -n $(which "$tool_cmd") ]]; then
    debug_output "- ${greenf}${tool_name} Installed${reset}"
    ret=0
  else
    debug_output "- ${redf}${tool_name} is not installed or is not set in the ${boldon}PATH${boldoff} variable${reset}"
    debug_output "  ${blueb}${yellowf}Install ${tool_name}${reset}"
    debug_output "  ${tool_website}"
    prereq_errors=$((prereq_errors+1))
    ret=1
  fi
  debug_output " "
  return $ret
}

debug_output() {
  if [[ $DEBUG == 1 ]]; then
    echo -e "${1}"
  fi
}

encrypt_file() {
  encrypted_file=$1
  decrypted_file=$2
  openssl rsautl -encrypt -inkey $HOME/.ssh/id_rsa -pubin -in $decrypted_file -out $encrypted_file
}


decrypt_file() {
  encrypted_file=$1
  decrypted_file=$2
  openssl rsautl -decrypt -inkey $HOME/.ssh/id_rsa -in $encrypted_file -out $decrypted_file
}

check_vmware_sdk() {
  check_tool_installed "VMware SDK 5.5" "vmtemplate.pl" "${purplef}https://code.pnmac.com/perl/vmware-sdk-5-5${reset}"
  if [ ! -f "/usr/lib/vmware-viperl/apps/vm/vmtemplate.pl" ]; then
    echo "Would you like to install VMware SDK 5.5? (y|n) : "
    sdk_install = read
    if [[ "$sdk_install" == "Y" || "$sdk_install" == "y" ]]; then
      git clone git@gitlab.pnmac.com:perl/vmware-sdk-5-5.git /tmp/vmware-sdk-5-5
      cd /tmp/vmware-sdk-5-5
      perl Makefile.PL
      make
      sudo make install
    fi
  fi

    if [ ! -f "${HOME}/.visdkrc" ]; then
      echo "Would you like to build your .visdkrc file? (y|n) : "
      read sdkrc_answer
      if [[ "${dkrc_answer}" == "Y" || "${sdkrc_answer}" == "y" ]]; then
        if [ -n "${vsphere_user}" ] && [ -n "${vsphere_pass}" ]; then
          echo -n "enter your sudo password: "
          sudo PERL_MM_USE_DEFAULT=1 perl -MCPAN -e 'install Crypt::SSLeay'

cat << EOL > $HOME/.visdkrc
VI_SERVER = vcs.pnmac.com
VI_USERNAME = $vsphere_user
VI_PASSWORD = $vsphere_pass
VI_PROTOCAL = https
VI_PORTNUMBER = 443

EOL

echo
chmod 600 $HOME/.visdkrc

        else
          echo -e "${redf}${boldon}vsphere_user${boldoff} and ${boldon}vsphere_pass${boldoff} are not set, unable to create $HOME/.visdkrc.  exiting!${reset}"
        fi
      fi
    fi
}

install_vghetto() {
  if [ ! -d "${HOME}/vghetto-scripts" ]; then
    git clone https://github.com/lamw/vghetto-scripts.git $HOME/vghetto-scripts
  fi
}

check_vsphere_variables() {
  debug_output "${yellowf}Checking VSphere variables${reset}"

  if [ -z "${vsphere_user}" ]; then
    debug_output "- ${redf}Varible ${boldon}vsphere_user${boldoff} is not set.  export the variable with your AD username (mrmeeseeks@pnmac.com)"
    debug_output "  ${blueb}${yellowf}export ${boldon}vsphere_user${boldoff}=${reset}"
    debug_output " "
    prereq_errors=$((prereq_errors+1))
  else
    debug_output "- ${greenf}Varible ${boldon}vsphere_user${boldoff} is set${reset}"
    debug_output " "
  fi

  if [ -z "${vsphere_pass}" ]; then
    debug_output "- ${redf}Varible ${boldon}vsphere_pass${boldoff} for user (00devops) is not set.  export the variable with the proper password${reset}"
    debug_output "  ${blueb}${yellowf}export ${boldon}vsphere_pass${boldoff}=${reset}"
    debug_output " "
    prereq_errors=$((prereq_errors+1))
  else
    debug_output "- ${greenf}Varible ${boldon}vsphere_pass${boldoff} is set${reset}"
    debug_output " "
  fi
}

check_prerequistites() {
  clear
  debug_output "\n${whiteb}${blackf}${ulon}Checking Prerequisites for building images with packer.${reset}\n"

  # packer-helper config
  # packer
  check_tool_installed "packer" "packer" "${purplef}http://www.packer.io/downloads.html${reset}"
  # end packer

  # homebrew
  check_tool_installed "homebrew" "brew" "${purplef}http://brew.sh${reset}"
  # end homebrew
  
  # ovftool
  check_tool_installed "ovftool" "ovftool" "${purplef}http://goo.gl/CnXeWS${reset}"
    if [ ! -f "${HOME}/.ovftool.ssldb" ]; then
      echo -e "  ${redf}You are missing the config file ${HOME}/.ovftool.ssldb${reset}"
      echo -en "  ${yellowf}Would you like to have it created?${reset} (y|n) : "
      read ovf_answer
      if [[ "${ovf_answer}" == "y" || "${answer}" == "Y" ]]; then
        echo -e "  ${greenf}creating ovfile${reset}"
cat << EOF > $HOME/.ovftool.ssldb
vcs.pnmac.com cert-sha1 3t/g+rRjw0H0JmZiRjKZQCdy/48=
EOF
      fi
  
    echo
  fi

  # end ovftool

  # aws cli
  check_tool_installed "aws cli" "aws" "${purplef}http://aws.amazon.com/cli${reset} or ${cyanf}brew install awscli${reset}"
  retval=$?
  if [ $retval -eq 0 ]; then
    if [ ! -f "${HOME}/.aws/config" ]; then
      echo -e "  ${redf}You are missing the config file ${HOME}/.aws/config${reset}"
      echo -en "  ${yellowf}Would you like to configure your aws cli configuration?${reset} (y|n) : "
      read aws_answer
      if [[ "${aws_answer}" == "y" || "${aws_answer}" = "Y" ]]; then
        echo
        echo -e "  ${purpleb}Make sure your IAM user has privilges to upload to vagrant.pnmac.com${reset}"
        echo
        aws configure
      fi
    fi

    if [ -f "${HOME}/.aws/config" ]; then
      export AWS_ACCESS_KEY_ID=$(awk 'toupper($1) ~ /AWS_ACCESS_KEY_ID/  {print $3}' ~/.aws/config)
      export AWS_SECRET_ACCESS_KEY=$(awk 'toupper($1) ~ /AWS_SECRET_ACCESS_KEY/  {print $3}' ~/.aws/config)
    fi
  fi

  # vmware information - variables firs then sdk to help build .visdkrc and satisfy packer needs
  check_vsphere_variables
  check_vmware_sdk
  install_vghetto
  
  # failed prerequisites message
  if [[ $prereq_errors > 0 ]]; then
    echo -e "You must fix the ${redf}${prereq_errors}${reset} missing prerequistites before you can continue."
    if [[ $DEBUG != 1 ]]; then
      echo "Run check prerequisites for detailed information"
    fi
    echo -e "\nexiting!\n"
    exit 2
  else
    if [[ $DEBUG == 1 ]]; then
      read -p "Press any key to continue"
    fi
  fi
  # end aws cli

}


run_packer() {
  clear
  echo -e "${blueb}Build an image with packer${reset}"
  echo 
  curr_dir=$(basename $(pwd))
  PS3="Which image would you like to build?: "
  select FILENAME in *.json; do
    packer build $FILENAME
    stack_name=$(awk '/\"stack_name\"\:/ {gsub(/\"/, "", $2); gsub(/\,/, "", $2); print $2}' $FILENAME)
    if [ $? -eq 0 ]; then
      stack_name=$( echo $FILENAME | awk -F. '{print $1}')
 
      # make sure the card is vmxnet3
      vmNICManagement.pl --nictype vmxnet3 --operation updatenictype --vnic 1 --vmname $stack_name

      # make the system a template
      vmtemplate.pl --operation T --vmname $stack_name

    else 
      echo -e "${redf}packer build failed!  exiting!${reset}"
    fi
  done
}

echo -e "${blueb}Packer helper${reset}"
echo
OPTIONS="build image,check prerequistites"
PS3="What would you like to do? : "
IFS=','; select OPT in $OPTIONS; do
  case $OPT in
    "$QUIT")
      echo "Exiting."
      break
      ;;
    "build image")
      DEBUG=0
      check_prerequistites
      run_packer
      exit 0
      ;;
    "check prerequistites")
      DEBUG=1
      check_prerequistites      
      ;;
    *)
      echo "Unknown option: Try try again!"
      ;;
  esac
  clear
done

exit 0
