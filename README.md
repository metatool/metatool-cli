metatool
====

What is it for
----
It is intended to be a client workflow wrapper and toolbox
- helping to maintain good profile standards
- exposing multiple languages in a portable way
- brings along a customizable toolbox to different operating systems (well not yet..but someday)
- it's not trying to reivent the wheel but facilitate many good wheels while adding the ability to maintain local workflows which in turn trigger remote workflows.

File Structure
---
The core lib section of metatool is written in bash but is structured as well as bash can be I guess.  libs/core should be a set of actions similar to private methods in OOP (but hey it's really just bash) while workflows in a similar vein are structured like public methods but are the stackable linear workflows that you might want.

    bin (metatool scripts)
    lib (supporting libraries for metatool)
     \_ chef (chef solo for managing dependencies)
     \_ metatool (metatool libs)
         \_ modules (modules that make up the metatool tool)
            \_ core (core libraries and predefined actions)
            \_ user (custom or third party modules - distribution to be worked out in the future)
         \_ workflow (new commands for metatool are made here for your local workflows)
            \_ core (workflows essential to using metqtool)
            \_ user (custom workflows that the user creates typically as they related to core/user modules
    tmp (temporary or disposable files isolated to your profile)
    etc (configuration stores for metatool and any other supporting tools it manages)
    
Compatibility
----

- This has been built and tested on OS X Mavericks
- Intent is to have this work on most common platforms (Linux,FreeBSD,Solaris,OS X) but more work to be done
- Please not this is in it's early stages and started as many ideas do to a set of problems I observe in my workplace or at home.  There are probably better solutions out there but creating somethings keeps me from going insanely mad.  I only hope it helps.


Installation
----

1. Clone this repository some place and make sure the bin directory is in your local PATH variable
2. metatool init
3. at this point you are either happy or sad because it worked or it didn't

TODO
----
- There is a liberal use of bash eval here... normally eval in is really a no no and should never be used.  Need to find out if there are better ways.  Since this is really a personal workflow system I'm less concerned about it but it lingers in the back of my mind.  Find a better way. 
- Add Ansible support so users have the option of managing via chef or ansible (virtualenv)
- Strip out Chef cookbooks and use berkshelf to deal with dependencies 
- config to select Chef or Ansible
- Pull modules into one repository...might need to get creative with git and remote listings (otherwise mature the modules foundations)
- json workflow management - create or find a utility that converts yaml to json.  It might not be a bad idea to curl it to http://jsontoyaml.com - yaml is way better to read
