workflow_test() {
    metatool.lib
    workflows.lib
    dialog.lib
    gitlab.lib "https://code.pnmac.com/api/v3"
    vcsh.lib "gitlab.com" "metatool"
    git.lib "gitlab.com"

    ##dialog_calendar "what day" "pick one"
    ##dialog_time "what time" "pick one"
    ##metatool_schedule_job "lock" "20"
    ##metatool_remove_job "lock"
    ##workflow_from_json "servicenow" "create_change_request.json"
    ##gitlab_get_groups
    ##metatool_list_modules
    ##metatool_create_config
    ##gitlab_get_projects "chef"

    ##metatool_get_module "aws"
    metatool_install_modules
}
