workflow_openssh() {
    openssh.lib
    dialog.lib

    openssh_private_public_keys
    openssh_private_pem
}
