workflow_update() {
    metatool.lib
    dialog.lib
    chef.lib    
    gitlab.lib

    metatool_update
    metatool_install_modules
    metatool_install_workflows
}
