workflow_git() {
    dialog.lib
    openssh.lib

    # workflow steps
    workflow_update
    workflow_openssh
}
