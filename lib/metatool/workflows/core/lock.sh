workflow_lock() {
    metatool.lib

    # lock metatool 
    # this is really about hiding stored credentials that are unencrypted
    # unfortunately openssh and ssh-keygent don't work together so this is
    # self managed solution.  Allow the credentials to remain unencrypted for
    # a scheduled period of time and the have a cron entry remove the
    # unencrypted file.  The next run of metatool will prompt the user for
    # their ssh key password decrypting it again for a period of time

    metatool_password_lock
    metatool_remove_job "job" "lock"
}
