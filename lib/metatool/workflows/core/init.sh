workflow_init() {
    metatool.lib
    dialog.lib
    openssh.lib
    gitlab.lib
 
    # install dependencies as needed
    metatool_install_dependencies
    metatool_create_config

    # workflow steps
    workflow_update
    workflow_openssh
    profile_init
    profile_link_to_shell
 
    #openssh_encrypt_file
    #local unencrypted_file=$1
    #local encrypted_file="${unencrypted_file}.enc"
}
